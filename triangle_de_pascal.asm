%include "io.inc"

section .data
line TIMES 127 dd 0 ;crÈe un espace mÈmoire pour 64 ÈlÈments (chacun stockÈ sur 2x32 bits) initialisÈ avec un 1 et des zÈros

section .text
global CMAIN
CMAIN:
    mov eax,1
    mov [line],eax ;init 1ere ligne du triangle
    mov edi, 1 ;pointe vers la ligne du triangle en mÈmoire

line_loop:
    inc edi     
    mov esi, 1 ;compteur du nombre d'elements ‡ leur place
    mov eax, 1 ;bits de poids faible (bdpfaible) du 0e element de chaque ligne dans eax
    mov ebx, 0 ;bits de poids fort (bdpfort) du 0e element de chaque ligne dans ebx
    cmp edi, 64 ;check si ligne 64 construite en mÈmoire
    jz init_print
    
elem_loop: ;ÈlÈment prÈcÈdent dans eax (bdpfaible) et ebx (bdpfort)
    mov ecx,[line+8*esi] ;element suivant dans les registres ecx et edx
    mov edx,[line+8*esi+4] 
    add eax,ecx ;somme des bdpfaible
    adc ebx,edx ;somme avec le reste des bdpfort
    mov [line+8*esi],eax ;placement du nombre calcule
    mov [line+8*esi+4],ebx
    add esi,1 ;element bien place supplementaire donc inc compteur
    cmp edi,esi ;check si ligne finie
    jz line_loop
    mov ecx,eax ;element precedent dans eax et ebx
    mov edx,ebx
    jnz elem_loop
    
init_print:
    mov esi,0 ;compteur du nombre d'elements affiches

print_line:
    PRINT_HEX 4, [line+8*esi+4]
    PRINT_CHAR " "
    PRINT_HEX 4, [line+8*esi+4]
    PRINT_CHAR " "
    inc esi

end:
    cmp esi,63
    jnz print_line
    ret
    